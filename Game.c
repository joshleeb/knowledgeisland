/*
 *  Game.c
 *  Josh, Sebastian, Kenneth, Roger
 *  COMP1917 Wed13-Tuba KnowledgeIsland
 */

#include <stdio.h>
#include <stdlib.h>
#include "Game.h"
#include <time.h>
#include <string.h>
#include <assert.h>

#define MAX_DICE_VAL 6
#define NUM_DICE_ROLLS 2
#define NUM_TILES 19
#define NUM_DISCIPLINES 6
//9 Retraining centres according to the appspot game
//#define NUM_RETRAIN_CENTER 9

typedef struct _boardElement {
    // from north = 0; clockwise. 1 if true
    int childExists[4];

    // In each of these, akin to player values. 0 = NO_ONE, 1 = UNI_A etc
    // Whereby if something != NO_ONE it is a campus/go8/arc belonging to that player
    int campusId;
    int go8Id;

    // Similar, however arcIds is unique for every ARC, arcPlayers is the equivalent player
    int arcIds[4];
    int arcPlayers[4];

    // Index of a discipline the vertex is adjacent to; -1 if doesn't exist
    int hexIndexes[3];
    int retrainCenter;

    // for debugging
    char contents;
} boardElement;

typedef struct _game {
    int currentTurn;

    int disciplines[NUM_TILES];
    int dice[NUM_TILES];

    //Format of arrays: e.g. KPI[UNI_A] will return the KPI of the first uni
    int KPI[NUM_UNIS];
    int numCampus[NUM_UNIS];
    int numGo8[NUM_UNIS];
    int numArcGrants[NUM_UNIS];
    int numPatents[NUM_UNIS];
    int numPapers[NUM_UNIS];

    int numStudents[NUM_UNIS][NUM_DISCIPLINES];

    //gets whether player has specific retraining centre
    // e.g. if retrainCenters[UNI_A-1][STUDENT_MJ] is 1, then UNI_A has a MJ retraining centre
    int retrainCenters[NUM_UNIS][NUM_DISCIPLINES];
    int globalARCcount;
    boardElement gameBoard[13][8];
} game;


// Board manipulation functions
void bottomLeftCorner( int *children );
void bottomRightCorner( int *children );
void downJunction( int *children );
void lineJunction( int *children );
void topLeftCorner( int *children );
void topRightCorner( int *children );
void upJunction( int *children );
void initializeBoard( Game g );
boardElement *getElementAtPath( Game g, char *in_path );
int clockwiseToX( int dir );
int clockwiseToY( int dir );
int deltaToClockwise( int dx, int dy );
void printBoard( Game g );
void exampleUsage( Game g );

// Nicer interface functions
// NOTE: In every case, the path MUST be valid. Use isValidPath()
// These functions are underscored to distinguish them from client-facing functions
void _placeARC(Game g, int player, char *path);
void _placeCampus(Game g, int player, char *path);
void _placeGO8(Game g, int player, char *path);
int _getARC(Game g, char *path);
int _getCampus(Game g, char *path);
int _getGO8(Game g, char *path);

static int isValidPath(Game g, char *path);

Game newGame(int discipline[], int dice[]){
    //psuedo-random seed for dice rolling later
    srand (time(NULL));

    Game theGame = (Game)malloc( sizeof( game ) );

    // Copy our discipline and dice positions to the game
    memcpy( &theGame->disciplines, discipline, NUM_TILES*sizeof(int) );
    memcpy( &theGame->dice, dice, NUM_TILES*sizeof(int) );

    theGame->globalARCcount = 0;
    theGame->currentTurn = -1;

    int i;
    for( i = 0; i != 3; ++i ) {
        theGame->KPI[i] = 20;
        theGame->numCampus[i] = 2;
        theGame->numGo8[i] = 0;
        theGame->numArcGrants[i] = 0;
        theGame->numPatents[i] = 0;
        theGame->numPapers[i] = 0;
    }

    int j;
    for( j = 0; j != 6; ++j ) {
        for( i = 0; i != 3; ++i ) {
            // initial students:
            // (1)BPS:3, (2)B?:3, (3)MTV:1, (4)MJ:1, (5)M$:1
            // i <-player, j <-student

            if (j == 1 || j == 2) {
                theGame->numStudents[i][j] = 3;
            } else if (j >= 3 && j <= 5) {
                theGame->numStudents[i][j] = 1;
            } else {
                theGame->numStudents[i][j] = 0;
            }
        }
    }

    //gets whether player has specific retraining centre
    // e.g. if retrainCenters[UNI_A-1][STUDENT_MJ] is 1, then UNI_A has a MJ retraining centre
    //TODO: INIT THIS PROPERLY
    // retrainCenters[NUM_UNIS][NUM_RETRAIN_CENTER];

    initializeBoard( theGame );

    return theGame;
}

#define DEFAULT_DISCIPLINES {STUDENT_BQN, STUDENT_MMONEY, STUDENT_MJ,\
    STUDENT_MMONEY, STUDENT_MJ, STUDENT_BPS, STUDENT_MTV,\
    STUDENT_MTV, STUDENT_BPS,STUDENT_MTV, STUDENT_BQN,\
    STUDENT_MJ, STUDENT_BQN, STUDENT_THD, STUDENT_MJ,\
    STUDENT_MMONEY, STUDENT_MTV, STUDENT_BQN, STUDENT_BPS }

#define DEFAULT_DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,7,9,2,8,10,5}

// XXX: Compile with this uncommented to test things
/*int main() {
    int default_ds[] = DEFAULT_DISCIPLINES;
    int default_dc[] = DEFAULT_DICE;
    Game g = newGame(default_ds, default_dc);
    printBoard(g);
    exampleUsage(g);
}*/

// Sebastian
void disposeGame (Game g) {
    // At the moment, game does not contain any sub-pointers so we can just free it
    // without having to worry about its contents.
    free( g );
}

// Roger
void makeAction (Game g, action a) {

    // UNI_A has ID 1, but array starts from index num 0
    int currentPlayer = getWhoseTurn (g) - 1;

    // record who had most before as it may change after action
    int currentMostARCs = getMostARCs (g);
    int currentMostPubs = getMostPublications (g);

    int count = 0;


    // Remember building campuses,etc. cost students

    if (a.actionCode == PASS) {
        // exit function, throwDice occurs in runGame.c
    } else if (a.actionCode == BUILD_CAMPUS) {
            // Students used
            /* obsolete, subject to removal
            g->numBPS[currentPlayer] -= 1;
            g->numBQN[currentPlayer] -= 1;
            g->numMJ[currentPlayer] -= 1;
            g->numMTV[currentPlayer] -= 1;
            */

        count = 1;
        // since BPS, BQN, MJ, MTV are 1-4 respectively
        while (count <= 4) {
            g->numStudents[currentPlayer][count] -= 1;
            count++;
        }

        // Campus built
        _placeCampus (g, currentPlayer+1, a.destination);

        // Update game campus data
        g->numCampus[currentPlayer] += 1;

        // KPI changes
        g->KPI[currentPlayer] += 10;
    } else if (a.actionCode == BUILD_GO8) {
        // Students used
        /* obsolete, subject to removal
            g->numMJ[currentPlayer] -= 2;
            g->numMMoney[currentPlayer] -= 3;
        */

        g->numStudents[currentPlayer][STUDENT_MJ] -= 2;
        g->numStudents[currentPlayer][STUDENT_MMONEY] -= 3;

        // Campus upgraded to G08 Campus
        _placeGO8 (g, currentPlayer+1, a.destination);
        // Update game g08 data
        g->numCampus[currentPlayer] -= 1;
        g->numGo8[currentPlayer] += 1;

        // KPI changes (+20 for GO8, -10 for upgrade)
        g->KPI[currentPlayer] += 10;
    } else if (a.actionCode == OBTAIN_ARC) {
        // Students used
        /* obsolete, subject to removal
            g->numBQN[currentPlayer] -= 1;
            g->numBPS[currentPlayer] -= 1;
        */

        count = 1;
        // since BPS and BQN are 1-2 respectively
        while (count <= 2) {
            g->numStudents[currentPlayer][count] -= 1;
            count++;
        }

        // ARC grant built
        _placeARC (g, currentPlayer+1, a.destination);

        // Update game ARC data
        g->numArcGrants[currentPlayer] += 1;

        // KPI changes
        g->KPI[currentPlayer] += 2;

    //} else if (a.actionCode == START_SPINOFF) {

            /* more obsolete stuff, moved to runGame.c
            int randomVal = rand();
            //store a random number 1, 2 or 3
            // if num 1, actionCode = Obtain IP
            // if num 2 or 3, actionCode = Obtain Publication

            // perhaps write a iterative test
            // to check whether about 1/3 is IP
            if (randomVal < RAND_MAX / 3) {
                a.actionCode = OBTAIN_IP_PATENT;
            } else {
                a.actionCode = OBTAIN_PUBLICATION;
            }
            */

            // Students used
            /* obsolete, subject to removal
            g->numMJ[currentPlayer] -= 1;
            g->numMTV[currentPlayer] -= 1;
            g->numMMoney[currentPlayer] -= 1;
            */

    } else if (a.actionCode == OBTAIN_PUBLICATION) {
        // students used
        // since MJ, MTV, MMoney are 3-5 respectively
        count = 3;
        while (count <= 5) {
            g->numStudents[currentPlayer][count] -= 1;
            count++;
        }

        // Update Game publication data
        g->numPapers[currentPlayer] += 1;

    } else if (a.actionCode == OBTAIN_IP_PATENT) {
        // students used
        count = 3;
        while (count <= 5) {
            g->numStudents[currentPlayer][count] -= 1;
            count++;
        }

        // Update Game patent data
        g->numPatents[currentPlayer] += 1;
        // KPI changes
        g->KPI[currentPlayer] += 10;

    } else if (a.actionCode == RETRAIN_STUDENTS) {
        //Affected by whether near training centres
        if (a.disciplineFrom != a.disciplineTo &&
            a.disciplineFrom != STUDENT_THD) {

            int rd = 3; // rd -> reduce

            // need to ensure player has enough students
            // figure out how retrain centre types are determined
            if (g->retrainCenters
               [currentPlayer][a.disciplineFrom] == 1) {
                rd = 2;
            }

            g->numStudents[currentPlayer][a.disciplineFrom] -= rd;
            g->numStudents[currentPlayer][a.disciplineTo] += 1;
        }

                /* obsolete, subject to removal
                if (a.disciplineFrom == STUDENT_BPS) {
                    g->numBPS[currentPlayer] -= 3;
                } else if (a.disciplineFrom == STUDENT_BQN) {
                    g->numBQN[currentPlayer] -= 3;
                } else if (a.disciplineFrom == STUDENT_MJ) {
                    g->numMJ[currentPlayer] -= 3;
                } else if (a.disciplineFrom == STUDENT_MTV) {
                    g->numMTV[currentPlayer] -= 3;
                } else if (a.disciplineFrom == STUDENT_MMONEY) {
                    g->numMMoney[currentPlayer] -= 3;
                }

                if (a.disciplineTo == STUDENT_BPS) {
                    g->numBPS[currentPlayer] += 1;
                } else if (a.disciplineTo == STUDENT_BQN) {
                    g->numBQN[currentPlayer] += 1;
                } else if (a.disciplineTo == STUDENT_MJ) {
                    g->numMJ[currentPlayer] += 1;
                } else if (a.disciplineTo == STUDENT_MTV) {
                    g->numMTV[currentPlayer] += 1;
                } else if (a.disciplineTo == STUDENT_MMONEY) {
                    g->numMMoney[currentPlayer] += 1;
                } else if (a.disciplineTo == STUDENT_THD) {
                    g->numTHD[currentPlayer] += 1;
                }
                */
    }


    // Prestige calculations
    if (getMostARCs (g) != NO_ONE) {
        if (currentMostARCs != NO_ONE) {
            g->KPI[currentMostARCs-1] -= 10;
        }
        g->KPI[getMostARCs (g)-1] += 10;
    }

    if (getMostPublications (g) != NO_ONE) {
        if (currentMostPubs != NO_ONE) {
            g->KPI[currentMostPubs-1] -= 10;
        }
        g->KPI[getMostPublications (g)-1] += 10;
    }

}

// Kenneth
void throwDice (Game g, int diceScore) {
    /*int nextPlayer = g->currentTurn + 1;
    nextPlayer = nextPlayer % 3;
    g->currentTurn = nextPlayer;*/

    g->currentTurn += 1;

    int i, j, k;
    int myDiceRoll = diceScore;

    // For every board element
    for( i = 0; i != 13; ++i ) {
        for( j = 0; j != 8; ++j ) {

            // For every adjacent hex index
            for( k = 0; k != 3; ++k ) {

                // -1 means no adjacent hex
                if( g->gameBoard[i][j].hexIndexes[k] != -1 ) {

                    // If the dice value at our adjacent hex index equals the dice roll
                    if( g->dice[ g->gameBoard[i][j].hexIndexes[k] ] == myDiceRoll ) {

                        // If someone has a campus there
                        if( g->gameBoard[i][j].campusId != NO_ONE ) {

                            //printf("Adding resources type %d to player: %d\n", g->disciplines[ g->gameBoard[i][j].hexIndexes[k] ], g->gameBoard[i][j].campusId);

                            int studentType = g->disciplines[ g->gameBoard[i][j].hexIndexes[k] ];

                            if( g->gameBoard[i][j].go8Id != NO_ONE ) {
                                g->numStudents[g->gameBoard[i][j].campusId-1][studentType] += 1;
                            }

                            g->numStudents[g->gameBoard[i][j].campusId-1][studentType] += 1;

                        }

                    }
                }
            }
        }
    }

    // Convert all student types to THDs on a roll of seven
    if( diceScore == 7 ) {
        int i;
        for( i = 0; i != 3; ++i ) {
            int totalToConvert = 0;
            totalToConvert += g->numStudents[i][STUDENT_MTV];
            totalToConvert += g->numStudents[i][STUDENT_MMONEY];

            g->numStudents[i][STUDENT_MTV] = 0;
            g->numStudents[i][STUDENT_MMONEY] = 0;

            g->numStudents[i][STUDENT_THD] += totalToConvert;
        }
    }
    //throw the dice - actually this should be done elsewhere
    /**
    int diceResult = 0;
    int i = 0;
    while (i < NUM_DICE_ROLLS){
        diceResult += generateDiceValue();
        i++;
    }
    **/
}

// Josh
int getDiscipline (Game g, int regionID) {
    return g->disciplines[regionID];
}

// Sebastian
int getDiceValue (Game g, int regionID) {
    return g->dice[regionID];
}

// Roger
int getMostARCs (Game g) {
    int playerIndex = 0;
    int maxARCs = 0;
    int playerID = NO_ONE;


    // Check if all players have same num of ARCs
    if (g->numArcGrants[UNI_A-1] == g->numArcGrants[UNI_B-1] &&
        g->numArcGrants[UNI_C-1] == g->numArcGrants[UNI_B-1]) {

        playerID = NO_ONE;
    } else {
        while (playerIndex < 3) {
            // if player at index 0 has most ARCs then it's UNI_A
            if (g->numArcGrants[playerIndex] > maxARCs) {
                maxARCs = g->numArcGrants[playerIndex];
                playerID = playerIndex + 1;
            }
            playerIndex++;
        }
    }

    return playerID;
}

// Kenneth
int getMostPublications (Game g) {
    int mostPublications = NO_ONE;
    int numA = g->numPapers[UNI_A-1];
    int numB = g->numPapers[UNI_B-1];
    int numC = g->numPapers[UNI_C-1];
    if (numA > numB && numA > numC){
        mostPublications = UNI_A;
    } else if (numB > numA && numB > numC){
        mostPublications = UNI_B;
    } else if (numC > numA && numC > numB){
        mostPublications = UNI_C;
    }
    return mostPublications;
}

// Josh
int getTurnNumber (Game g) {
    return g->currentTurn;
}

// Sebastian
int getWhoseTurn (Game g) {
    if( getTurnNumber(g) < 0) {
        return NO_ONE;
    } else {
        // 1, 2, 3, 1, 2, 3....
        // Natively #defined to UNI_A, UNI_B etc
        return 1 + getTurnNumber(g) % 3;
    }
}

// Roger
// Before using such functions, need to call isValidPath first
int getCampus (Game g, path pathToVertex) {
    /*
    boardElement elt;
    elt = *getElementAtPath (g, pathToVertex);
    return elt.campusId;
    */
    return getElementAtPath (g, pathToVertex)->campusId;
}

// Kenneth
int getARC(Game g, path pathToEdge) {
    assert(isValidPath(g, pathToEdge) == TRUE);
    return _getARC(g, pathToEdge);
}

// together
int isLegalAction (Game g, action a) {
    int isLegal = TRUE;
    int player = getWhoseTurn(g);


    // -- all actions -----------------------------------------------
    // is valid action code
    isLegal = a.actionCode <= 7 && a.actionCode >= 0;

    //Nothing is legal until turn 0
    if (isLegal){
        if(g->currentTurn == -1){
            isLegal = FALSE;
        }
    }

    // is valid path
    if (isLegal && (a.actionCode == BUILD_CAMPUS || a.actionCode == BUILD_GO8 || a.actionCode == OBTAIN_ARC)  ) {
        isLegal = isValidPath(g, a.destination);
    }

    // is valid disciplines (if retratining students)
    if (isLegal && a.actionCode == RETRAIN_STUDENTS) {
        isLegal = a.disciplineFrom <= 5 && a.disciplineFrom >= 0 &&
            a.disciplineTo <= 5 && a.disciplineTo >= 0;
    }

    // university has sufficient students (if retraining students)
    if (isLegal && a.actionCode == RETRAIN_STUDENTS) {
        int numStudents = g->numStudents[player-1][a.disciplineFrom];
        int exchangeRate = getExchangeRate(g, player, a.disciplineFrom,
                a.disciplineTo);
        if (numStudents < exchangeRate){
            isLegal = FALSE;
        }
    }


    // -- university can place: -------------------------------------
    // a campus
    if (isLegal && a.actionCode == BUILD_CAMPUS) {
        int campusOwner = getCampus(g, a.destination);
        if (campusOwner != NO_ONE){
            isLegal = FALSE;
        }
        int currentPlayer = getWhoseTurn(g);
        
        
        char pathToLeft[PATH_LIMIT+1] = {0};
        strcpy(pathToLeft, a.destination);
        strcat(pathToLeft, "L");
        if (isValidPath(g, pathToLeft)){
            if (getCampus(g, pathToLeft) == currentPlayer){
                isLegal = FALSE;
            }
        }
        char pathToRight[PATH_LIMIT+1] = {0};
        strcpy(pathToRight, a.destination);
        strcat(pathToRight, "R");
        if (isValidPath(g, pathToRight)){
            if (getCampus(g, pathToRight) == currentPlayer){
                isLegal = FALSE;
            }
        }
        char pathToBack[PATH_LIMIT+1] = {0};
        strcpy(pathToBack, a.destination);
        pathToBack[strlen(a.destination)-1] = '\0';
        if (isValidPath(g, pathToBack)){
            if (getCampus(g, pathToBack) == currentPlayer){
                isLegal = FALSE;
            }
        }
        
    }

    // an arc
    if (isLegal && a.actionCode == OBTAIN_ARC) {
        int ARCowner = getARC(g, a.destination);
        if (ARCowner != NO_ONE){
            isLegal = FALSE;
        }
    }

    // upgrading to a go8 campus
    if (isLegal && a.actionCode == BUILD_GO8) {
        int campusExists = FALSE;
        if (getCampus(g, a.destination) == player){
            campusExists = TRUE;
        }
        int enoughStudents = FALSE;
        if (g->numStudents[player-1][STUDENT_MJ] >= 2 && g->numStudents[player-1][STUDENT_MMONEY] >= 2){
            enoughStudents = TRUE;
        }

        if (campusExists == FALSE || enoughStudents == FALSE){
            isLegal = FALSE;
        }
    }


    // -- university has sufficient resources if: -------------------
    // building a campus
    if (isLegal && a.actionCode == BUILD_CAMPUS) {
        isLegal = getStudents(g, player, STUDENT_BPS) >= 1
               && getStudents(g, player, STUDENT_BQN) >= 1
               && getStudents(g, player, STUDENT_MJ) >= 1
               && getStudents(g, player, STUDENT_MTV >= 1);
    }

    // obtaining an arc
    if (isLegal && a.actionCode == OBTAIN_ARC) {
        isLegal = getStudents(g, player, STUDENT_BPS) >= 1
               && getStudents(g, player, STUDENT_BQN) >= 1;
    }

    // upgrading to a go8 campus
    if (isLegal && a.actionCode == BUILD_GO8) {
        isLegal = getStudents(g, player, STUDENT_MJ) >= 2
               && getStudents(g, player, STUDENT_MMONEY >= 3);
    }

    // starting a business spinoff
    if (isLegal && a.actionCode == START_SPINOFF) {
        isLegal = getStudents(g, player, STUDENT_MJ) >= 1
               && getStudents(g, player, STUDENT_MTV >= 1)
               && getStudents(g, player, STUDENT_MMONEY >= 1);
    }
    
    //OBTAIN_PUBLICATION and OBTAIN_IP_PATENT can only be done thru spinoff, not directly
    if (isLegal && (a.actionCode == OBTAIN_PUBLICATION || a.actionCode == OBTAIN_IP_PATENT)) { 
        isLegal = FALSE;
    }
    return isLegal;
}

static int isValidPath(Game g, char *path) {
    // a path is valid if it contains only the characters 'L', 'B' and
    // 'R' and the path exists on the graph
    int isValid = TRUE;
    int i = 0;

    while (path[i] != '\0' && isValid == TRUE) {
        if (path[i] != 'R' && path[i] != 'L' && path[i] != 'B') {
            isValid = FALSE;
        }
        ++i;
    }

    // the path exists on the graph
    if (isValid) {
        // Trying to get an element will null if path doesn't exist
        isValid = ( getElementAtPath( g, path ) != 0 );
    }

    return isValid;
}

// Josh
int getKPIpoints (Game g, int player) {
    return g->KPI[player-1];
}

// Sebastian
int getARCs (Game g, int player) {
    return g->numArcGrants[player-1];
}

// Roger
int getGO8s (Game g, int player) {
    return g->numGo8[player-1];
}

// Kenneth
int getCampuses (Game g, int player) {
    return g->numCampus[player-1];
}

// Josh
int getIPs (Game g, int player) {
    return g->numPatents[player-1];
}

// Sebastian
int getPublications (Game g, int player) {
    return g->numPapers[player-1];
}

// Roger
int getStudents (Game g, int player, int discipline) {
    /*
    int numStudents;
    if (discipline == STUDENT_THD) {
        numStudents = g->numTHD[player-1];
    } else if (discipline == STUDENT_BPS) {
        numStudents = g->numBPS[player-1];
    } else if (discipline == STUDENT_BQN) {
        numStudents = g->numBQN[player-1];
    } else if (discipline == STUDENT_MJ) {
        numStudents = g->numMJ[player-1];
    } else if (discipline == STUDENT_MTV) {
        numStudents = g->numMTV[player-1];
    } else if (discipline == STUDENT_MMONEY) {
        numStudents = g->numMMoney[player-1];
    }
    return numStudents;
    */

    return g->numStudents[player-1][discipline];
}

// Kenneth
int getExchangeRate (Game g, int player, int disciplineFrom, int disciplineTo) {
    int studentsRequired = 3; //default rate
    if (g->retrainCenters[player-1][disciplineFrom] == 1){
        studentsRequired = 2;
    }
    return studentsRequired;
}

void exampleUsage(Game g) {

    // NOTE: None of these functions increment player num_arcs or n_campuses etc
    // This is why they start with underscores; because all they do
    // is raw board manipulation.

    // Placing a campus, or reading for a campus at a path
    _placeCampus(g, UNI_A, "RLL");
    assert( _getCampus(g, "RLL") != NO_ONE );
    assert( _getCampus(g, "RLL") == UNI_A );

    // Placing or reading a GO8 using a path
    _placeGO8(g, UNI_C, "RLLR");
    assert( _getGO8(g, "RLLR") != NO_ONE );
    assert( _getGO8(g, "RLLR") == UNI_C );

    // Placing or reading an ARC using a path
    _placeARC(g, UNI_B, "RL");
    assert( _getARC(g, "RL") != NO_ONE );
    assert( _getARC(g, "RL") == UNI_B );

    _placeARC(g, UNI_A, "RLLRL");
    assert( _getARC(g, "RLLRL") != NO_ONE );
    assert( _getARC(g, "RLLRL") == UNI_A );

    // NOTE that the placing of campuses above changes the results of the below code :D

    // IMPORTANT EXAMPLE USAGE
    // Example which will increment student count of unis with a campus near a dice value

    int i, j, k;
    int myDiceRoll = 11;

    // For every board element
    for( i = 0; i != 13; ++i ) {
        for( j = 0; j != 8; ++j ) {

            // For every adjacent hex index
            for( k = 0; k != 3; ++k ) {

                // -1 means no adjacent hex
                if( g->gameBoard[i][j].hexIndexes[k] != -1 ) {

                    // If the dice value at our adjacent hex index equals the dice roll
                    if( g->dice[ g->gameBoard[i][j].hexIndexes[k] ] == myDiceRoll ) {

                        // If someone has a campus there
                        if( g->gameBoard[i][j].campusId != NO_ONE ) {

                            printf("Adding resources type %d to player: %d\n", g->disciplines[ g->gameBoard[i][j].hexIndexes[k] ], g->gameBoard[i][j].campusId);

                            if( g->disciplines[ g->gameBoard[i][j].hexIndexes[k] ] == STUDENT_BPS ) {
                                // FIXME: numBPS no longer exists in struct
                                /*g->numBPS[ g->gameBoard[i][j].campusId-1 ] += 1;*/
                            }

                            if( g->disciplines[ g->gameBoard[i][j].hexIndexes[k] ] == STUDENT_BQN ) {
                                // FIXME: numBQN no longer exists in struct
                                /*g->numBQN[ g->gameBoard[i][j].campusId-1 ] += 1;*/
                            }

                            //... .. etc for all student types
                        }

                    }
                }
            }
        }
    }
}

void _placeARC(Game g, int player, char *path) {
    if( path[0] == '\0' ) {
        printf("Tried to place ARC with zero path!\n");
        return;
    }

    ++g->globalARCcount; // starts at 1

    // Get first vertex
    boardElement *el1 = getElementAtPath(g, path);

    // Cull off last movement instruction to get second-last vertex
    char second[PATH_LIMIT];
    strcpy(second, path);
    second[ strlen(second)-1 ] = '\0';

    boardElement *el2 = getElementAtPath(g, second);

    // Find first empty indexes
    int firstEmptyIndex = 0;
    while( el1->arcIds[firstEmptyIndex] != 0 ) ++firstEmptyIndex;

    int secondEmptyIndex = 0;
    while( el2->arcIds[secondEmptyIndex] != 0 ) ++secondEmptyIndex;

    // Fill 'em up
    el1->arcIds[firstEmptyIndex] = g->globalARCcount;
    el2->arcIds[secondEmptyIndex] = g->globalARCcount;

    el1->arcPlayers[firstEmptyIndex] = player;
    el2->arcPlayers[secondEmptyIndex] = player;

}

void _placeCampus(Game g, int player, char *path) {
    getElementAtPath(g, path)->campusId = player;
    getElementAtPath(g, path)->contents = 'a'+(player-1);

    //check if there is a retraining centre
    int centerType = getElementAtPath(g, path)->retrainCenter;
    if (centerType != -1){
        g->retrainCenters[player-1][centerType] = 1;
    }
}

void _placeGO8(Game g, int player, char *path) {
    getElementAtPath(g, path)->go8Id = player;
    getElementAtPath(g, path)->contents = 'A'+(player-1);
}

int _getARC(Game g, char *path) {
    if( path[0] == '\0' ) {
        printf("Tried to get ARC with zero path!\n");
        return NO_ONE;
    }

    // Get first vertex
    boardElement *el1 = getElementAtPath(g, path);

    // Cull off last movement instruction to get second-last vertex
    char second[PATH_LIMIT];
    strcpy(second, path);
    second[ strlen(second)-1 ] = '\0';

    boardElement *el2 = getElementAtPath(g, second);

    // Look for a common ARC id between the two vertices
    int i, j;
    int commonIndex = -1;
    for( i = 0; i != 4; ++i ) {
        int toSearch = el1->arcIds[i];
        for( j = 0; j != 4; ++j ) {
            if( el2->arcIds[j] == toSearch && el2->arcIds[j] != NO_ONE ) {
                commonIndex = i;
            }
        }
    }

    // No common index? No ARC.
    if( commonIndex == -1 ) {
        return NO_ONE;
    } else {
        // Otherwise return the player ID
        return el1->arcPlayers[commonIndex];
    }
}

int _getCampus(Game g, char *path) {
    return getElementAtPath(g, path)->campusId;
}

int _getGO8(Game g, char *path) {
    return getElementAtPath(g, path)->go8Id;
}

void downJunction( int *children ) {
    children[1] = 1;
    children[2] = 1;
    children[3] = 1;
}

void upJunction( int *children ) {
    children[0] = 1;
    children[1] = 1;
    children[3] = 1;
}

void lineJunction( int *children ) {
    children[1] = 1;
    children[3] = 1;
}

void topLeftCorner( int *children ) {
    children[1] = 1;
    children[2] = 1;
}

void topRightCorner( int *children ) {
    children[2] = 1;
    children[3] = 1;
}

void bottomLeftCorner( int *children ) {
    children[0] = 1;
    children[1] = 1;
}

void bottomRightCorner( int *children ) {
    children[0] = 1;
    children[3] = 1;
}

void assignDisciplines( boardElement *el, int d1, int d2, int d3 ) {
    el->hexIndexes[0] = d1;
    el->hexIndexes[1] = d2;
    el->hexIndexes[2] = d3;
}

void initializeBoard( Game g ) {
    int i;
    // i is x (first index)
    for( i = 0; i != 13; ++i ) {

        // set all to null and then perform board setup
        int j;
        for( j = 0; j != 8; ++j ) {
            int k;
            g->gameBoard[i][j].contents = 'E';
            g->gameBoard[i][j].campusId = NO_ONE;
            g->gameBoard[i][j].go8Id = NO_ONE;
            g->gameBoard[i][j].retrainCenter = -1;

            for( k = 0; k != 4; ++k ) {
                g->gameBoard[i][j].childExists[k] = 0;
                g->gameBoard[i][j].arcIds[k] = 0;
                g->gameBoard[i][j].arcPlayers[k] = NO_ONE;
            }

            for( k = 0; k != 3; ++k ) {
                g->gameBoard[i][j].hexIndexes[k] = -1;
            }

            // We're on the actual board tiles
            // NOTE: Children should ALWAYS be listed clockwise
            if( j == 1 ) {
                topLeftCorner( g->gameBoard[3][j].childExists ); assignDisciplines(&g->gameBoard[3][j], 7, -1, -1);
                lineJunction( g->gameBoard[4][j].childExists ); assignDisciplines(&g->gameBoard[4][j], 7, -1, -1);
                downJunction( g->gameBoard[5][j].childExists ); assignDisciplines(&g->gameBoard[5][j], 7, 12, -1);
                g->gameBoard[5][j].retrainCenter = STUDENT_MMONEY;
                lineJunction( g->gameBoard[6][j].childExists ); assignDisciplines(&g->gameBoard[6][j], 12, -1, -1);
                g->gameBoard[6][j].retrainCenter = STUDENT_MMONEY;
                downJunction( g->gameBoard[7][j].childExists ); assignDisciplines(&g->gameBoard[7][j], 12, 16, -1);
                lineJunction( g->gameBoard[8][j].childExists ); assignDisciplines(&g->gameBoard[8][j], 16, -1, -1);
                topRightCorner( g->gameBoard[9][j].childExists ); assignDisciplines(&g->gameBoard[9][j], 16, -1, -1);
            }

            if( j == 2 ) {
                topLeftCorner( g->gameBoard[2][j].childExists ); assignDisciplines(&g->gameBoard[2][j], 3, -1, -1);
                g->gameBoard[2][j].retrainCenter = STUDENT_MTV;
                upJunction( g->gameBoard[3][j].childExists ); assignDisciplines(&g->gameBoard[3][j], 3, 7, -1);
                g->gameBoard[2][j].retrainCenter = STUDENT_MTV;
                downJunction( g->gameBoard[4][j].childExists ); assignDisciplines(&g->gameBoard[4][j], 3, 7, 8);
                upJunction( g->gameBoard[5][j].childExists ); assignDisciplines(&g->gameBoard[5][j], 7, 8, 12);
                downJunction( g->gameBoard[6][j].childExists ); assignDisciplines(&g->gameBoard[6][j], 8, 12, 13);
                upJunction( g->gameBoard[7][j].childExists ); assignDisciplines(&g->gameBoard[7][j], 12, 13, 16);
                downJunction( g->gameBoard[8][j].childExists ); assignDisciplines(&g->gameBoard[8][j], 13, 16, 17);
                upJunction( g->gameBoard[9][j].childExists ); assignDisciplines(&g->gameBoard[9][j], 16, 17, -1);
                topRightCorner( g->gameBoard[10][j].childExists ); assignDisciplines(&g->gameBoard[10][j], 17, -1, -1);
                g->gameBoard[10][j].retrainCenter = STUDENT_BQN;
            }

            if( j == 3 ) {
                topLeftCorner( g->gameBoard[1][j].childExists ); assignDisciplines(&g->gameBoard[1][j], 0, -1, -1);
                upJunction( g->gameBoard[2][j].childExists ); assignDisciplines(&g->gameBoard[2][j], 0, 3, -1);
                downJunction( g->gameBoard[3][j].childExists ); assignDisciplines(&g->gameBoard[3][j], 0, 3, 4);
                upJunction( g->gameBoard[4][j].childExists ); assignDisciplines(&g->gameBoard[4][j], 3, 4, 8);
                downJunction( g->gameBoard[5][j].childExists ); assignDisciplines(&g->gameBoard[5][j], 4, 8, 9);
                upJunction( g->gameBoard[6][j].childExists ); assignDisciplines(&g->gameBoard[6][j], 8, 9, 13);
                downJunction( g->gameBoard[7][j].childExists ); assignDisciplines(&g->gameBoard[7][j], 9, 13, 14);
                upJunction( g->gameBoard[8][j].childExists ); assignDisciplines(&g->gameBoard[8][j], 13, 14, 17);
                downJunction( g->gameBoard[9][j].childExists ); assignDisciplines(&g->gameBoard[9][j], 14, 17, 18);
                upJunction( g->gameBoard[10][j].childExists ); assignDisciplines(&g->gameBoard[10][j], 17, 18, -1);
                g->gameBoard[10][j].retrainCenter = STUDENT_BQN;
                topRightCorner( g->gameBoard[11][j].childExists ); assignDisciplines(&g->gameBoard[11][j], 18, -1, -1);
            }

            if( j == 4 ) {
                bottomLeftCorner( g->gameBoard[1][j].childExists ); assignDisciplines(&g->gameBoard[1][j], 0, -1, -1);
                downJunction( g->gameBoard[2][j].childExists ); assignDisciplines(&g->gameBoard[2][j], 0, 1, -1);
                upJunction( g->gameBoard[3][j].childExists ); assignDisciplines(&g->gameBoard[3][j], 0, 1, 4);
                downJunction( g->gameBoard[4][j].childExists ); assignDisciplines(&g->gameBoard[4][j], 1, 4, 5);
                upJunction( g->gameBoard[5][j].childExists ); assignDisciplines(&g->gameBoard[5][j], 4, 5, 9);
                downJunction( g->gameBoard[6][j].childExists ); assignDisciplines(&g->gameBoard[6][j], 5, 9, 10);
                upJunction( g->gameBoard[7][j].childExists ); assignDisciplines(&g->gameBoard[7][j], 9, 10, 14);
                downJunction( g->gameBoard[8][j].childExists ); assignDisciplines(&g->gameBoard[8][j], 10, 14, 15);
                upJunction( g->gameBoard[9][j].childExists ); assignDisciplines(&g->gameBoard[9][j], 14, 15, 18);
                downJunction( g->gameBoard[10][j].childExists ); assignDisciplines(&g->gameBoard[10][j], 15, 18, -1);
                g->gameBoard[10][j].retrainCenter = STUDENT_MJ;
                bottomRightCorner( g->gameBoard[11][j].childExists ); assignDisciplines(&g->gameBoard[11][j], 18, -1, -1);
            }

            if( j == 5 ) {
                bottomLeftCorner( g->gameBoard[2][j].childExists ); assignDisciplines(&g->gameBoard[2][j], 1, -1, -1);
                downJunction( g->gameBoard[3][j].childExists ); assignDisciplines(&g->gameBoard[3][j], 1, 2, -1);
                upJunction( g->gameBoard[4][j].childExists ); assignDisciplines(&g->gameBoard[4][j], 1, 2, 5);
                downJunction( g->gameBoard[5][j].childExists ); assignDisciplines(&g->gameBoard[5][j], 2, 5, 6);
                upJunction( g->gameBoard[6][j].childExists ); assignDisciplines(&g->gameBoard[6][j], 5, 6, 10);
                downJunction( g->gameBoard[7][j].childExists ); assignDisciplines(&g->gameBoard[7][j], 6, 10, 11);
                upJunction( g->gameBoard[8][j].childExists ); assignDisciplines(&g->gameBoard[8][j], 10, 11, 15);
                downJunction( g->gameBoard[9][j].childExists ); assignDisciplines(&g->gameBoard[9][j], 11, 15, -1);
                bottomRightCorner( g->gameBoard[10][j].childExists ); assignDisciplines(&g->gameBoard[10][j], 15, -1, -1);
                g->gameBoard[10][j].retrainCenter = STUDENT_MJ;
            }

            if( j == 6 ) {
                bottomLeftCorner( g->gameBoard[3][j].childExists ); assignDisciplines(&g->gameBoard[3][j], 2, -1, -1);
                lineJunction( g->gameBoard[4][j].childExists ); assignDisciplines(&g->gameBoard[4][j], 2, -1, -1);
                g->gameBoard[4][j].retrainCenter = STUDENT_BPS;
                upJunction( g->gameBoard[5][j].childExists ); assignDisciplines(&g->gameBoard[5][j], 2, 6, -1);
                g->gameBoard[5][j].retrainCenter = STUDENT_BPS;
                lineJunction( g->gameBoard[6][j].childExists ); assignDisciplines(&g->gameBoard[6][j], 6, -1, -1);
                upJunction( g->gameBoard[7][j].childExists ); assignDisciplines(&g->gameBoard[7][j], 6, 11, -1);
                lineJunction( g->gameBoard[8][j].childExists ); assignDisciplines(&g->gameBoard[8][j], 11, -1, -1);
                bottomRightCorner( g->gameBoard[9][j].childExists ); assignDisciplines(&g->gameBoard[9][j], 11, -1, -1);
            }
        }
    }

    // Place the initial campuses

    _placeCampus(g, UNI_A, "");
    _placeCampus(g, UNI_B, "RRLRL");
    _placeCampus(g, UNI_C, "RRLRLLRLRL");
    _placeCampus(g, UNI_C, "LRLRL");
    _placeCampus(g, UNI_B, "LRLRLRRLRL");
    _placeCampus(g, UNI_A, "LRLRLRRLRLRRLRL");
}

// Pretty print the board (good for debugging)
void printBoard( Game g ) {
    int i, j;
    for( i = 0; i != 8; ++i ) {
        for( j = 0; j != 13; ++j ) {
            if( g->gameBoard[j][i].childExists[0] ) {
                printf(" | ");
            } else {
                printf("   ");
            }
        }

        printf("\n");
        for( j = 0; j != 13; ++j ) {
            if( g->gameBoard[j][i].childExists[3] ) {
                printf("-%c",g->gameBoard[j][i].contents);
            } else {
                printf(" %c",g->gameBoard[j][i].contents);
            }
            if( g->gameBoard[j][i].childExists[1] ) {
                printf("-");
            } else {
                printf(" ");
            }
        }
        printf("\n");

        for( j = 0; j != 13; ++j ) {
            if( g->gameBoard[j][i].childExists[2] ) {
                printf(" | ");
            } else {
                printf("   ");
            }
        }
        printf("\n");
    }
}

// Turn a position difference into a rotation from 0-3
int deltaToClockwise( int dx, int dy ) {
    if( dx == 0 && dy == 0 ) {
        printf( "clockwise fail" );
    }

    if( dx == 0 && dy == -1 ) return 0;
    if( dx == 1 && dy == 0 ) return 1;
    if( dx == 0 && dy == 1 ) return 2;
    if( dx == -1 && dy == 0 ) return 3;

    return 0;
}

// Turn rotation back into x difference
int clockwiseToX( int dir ) {
    if( dir == 0 || dir == 2 ) return 0;
    if( dir == 1 ) return 1;
    if( dir == 3 ) return -1;

    return 0;
}

// Turn rotation back into y difference
int clockwiseToY( int dir ) {
    if( dir == 1 || dir == 3 ) return 0;
    if( dir == 0 ) return -1;
    if( dir == 2 ) return 1;

    return 0;
}

// This function will return an element on the board, or null if the path runs off the board.
boardElement *getElementAtPath( Game g, char *in_path ) {

    char path[PATH_LIMIT];

    // Make a local copy of the path in case we need to fiddle with it
    strcpy( path, in_path );

    // Previous position is just 'above' A
    int prev_x = 2;
    int prev_y = 1;

    // Start navigating at A
    int current_x = 3;
    int current_y = 1;

    int index = 0;

    int validPathFound = 1;

    // Iterate through all the path instructions
    while( path[index] != '\0' ) {

        if( path[index] == 'L' ) {
            // direction that is pointing back from target vertex
            int dirBack = deltaToClockwise(prev_x- current_x, prev_y - current_y);
            int dir = dirBack;

            // Iterate clockwise a maximum of 180 degrees
            int i;
            for( i = 0; i != 2; ++i ) {
                ++dir;
                if( dir > 3 ) dir = 0; // wrap direction
                if( g->gameBoard[current_x][current_y].childExists[dir] ) {
                    //printf("Left found %d!", i);
                    break;
                }
            }

            // If there's no destination after iterating 180 degrees, a left turn doesn't exist
            if( !g->gameBoard[current_x][current_y].childExists[dir] ) {
                //printf("No valid left found!");
                validPathFound = 0;
                break;
            } else {
                // Otherwise update to the new position
                prev_x = current_x;
                prev_y = current_y;
                current_x += clockwiseToX(dir);
                current_y += clockwiseToY(dir);
            }
        }

        if( path[index] == 'R' ) {
            int dirBack = deltaToClockwise(prev_x- current_x, prev_y - current_y);
            int dir = dirBack;

            int i;
            for( i = 0; i != 2; ++i ) {
                --dir;
                if( dir < 0 ) dir = 3; // wrap direction
                if( g->gameBoard[current_x][current_y].childExists[dir] ) {
                    //printf("Right found %d!", i);
                    break;
                }
            }

            if( !g->gameBoard[current_x][current_y].childExists[dir] ) {
                //printf("No valid right found!");
                validPathFound = 0;
                break;
            } else {
                prev_x = current_x;
                prev_y = current_y;
                current_x += clockwiseToX(dir);
                current_y += clockwiseToY(dir);
            }
        }

        if( path[index] == 'B' ) {
            int temp_x = prev_x;
            int temp_y = prev_y;
            prev_x = current_x;
            prev_y = current_y;
            current_x = temp_x;
            current_y = temp_y;
            //printf("Direction reversed!");
        }

        //printf(" Traversed to: x: %d, y: %d\n", current_x, current_y);
        ++index;
    }

    boardElement *out = 0;

    if( validPathFound )
        out = &g->gameBoard[current_x][current_y];

    return out;
}
