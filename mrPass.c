/*
 *  Mr Pass.  Brain the size of a planet!
 *
 *  Proundly Created by Richard Buckland
 *  Share Freely Creative Commons SA-BY-NC 3.0. 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "Game.h"
#include "mechanicalTurk.h"

action decideAction (Game g) {

    action nextAction;
    nextAction.actionCode = PASS;

    int currentPlayer = getWhoseTurn(g); 
    
    if( currentPlayer != NO_ONE ) {
        
        int nMJ =  getStudents(g, currentPlayer, STUDENT_MJ),
            nMTV = getStudents(g, currentPlayer, STUDENT_MTV),
            nMM =  getStudents(g, currentPlayer, STUDENT_MMONEY);

        if( nMJ == 1 && nMTV == 1 && nMM == 1 ) {
            nextAction.actionCode = START_SPINOFF;
        }
        
    }

    return nextAction;
}
