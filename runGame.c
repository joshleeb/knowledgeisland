/*
 *  rungame.c
 *  Josh, Sebastian, Roger, Kenneth
 *  COMP1917 Wed13-Tuba KnowledgeIsland
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "Game.h"
#include "mechanicalTurk.h"

#define DEFAULT_DISCIPLINES { STUDENT_BQN, STUDENT_MMONEY, STUDENT_MJ, STUDENT_MMONEY, STUDENT_MJ, STUDENT_BPS, STUDENT_MTV, STUDENT_MTV, STUDENT_BPS,STUDENT_MTV, STUDENT_BQN, STUDENT_MJ, STUDENT_BQN, STUDENT_THD, STUDENT_MJ, STUDENT_MMONEY, STUDENT_MTV, STUDENT_BQN, STUDENT_BPS }

#define DEFAULT_DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,7,9,2,8,10,5}
#define WINNING_KPI 150
#define MAX_DICE_VAL 12

int rollDice();
int advancePlayer(int currentPlayer);
void printStats();
action doSpinoff(action move);

int main(int argc, char *argv[]) {
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    int currentPlayer = UNI_A;
    int diceValue = -1;

    Game g = newGame(disciplines, dice);

    while (getKPIpoints(g, currentPlayer) < WINNING_KPI) {
        diceValue = rollDice() + rollDice();     // simulate throw of the dice
        throwDice(g, diceValue);
        action move = decideAction(g);  // implemented in mechanicalTurk.c

        // loop until player action is PASS
        while (move.actionCode != PASS) {
            assert(isLegalAction(g, move) == TRUE);
            if (move.actionCode == START_SPINOFF) {
                //you may need to verify/fix this
                action spinoff = doSpinoff(move);
            }

            makeAction(g, move);
            move = decideAction(g);
        }

        currentPlayer = advancePlayer(currentPlayer);   // cycle through UNIs A, B and C
    }

    // print statistics
    printStats(g, currentPlayer);

    // free memory
    disposeGame(g);

    return EXIT_SUCCESS;
}

action doSpinoff(action move){
    int randNum = rand() % 3;

    //simulation of events with 2/3 and 1/3 probabilities
    if (randNum < 2){
        move.actionCode = OBTAIN_PUBLICATION;
    }else{
        move.actionCode = OBTAIN_IP_PATENT;
    }
    return move;
}

void printStats(Game g, int winner){
    int i = 0;
    char *players[3] = {"UNI_A", "UNI_B", "UNI_C"};
    printf("The winner is: %s\n", players[winner]);

    while (i < NUM_UNIS) {
        printf("Statistics for %s:\n", players[i]);
        printf("KPI is %d\n", getKPIpoints(g, i));
        printf("Number of ARCs is %d\n", getARCs(g, i));
        printf("KPI is %d\n", getCampuses(g, i));
        printf("Number of patents is is %d\n", getIPs(g, i));
        printf("Number of publications is %d\n", getPublications(g, i));
    }
}

int rollDice() {
    int diceValue = 1 + rand() % MAX_DICE_VAL;

    return diceValue;
}

int advancePlayer(int currentPlayer) {
    int nextPlayer = currentPlayer + 1;
    if (nextPlayer == 4) {
        nextPlayer = UNI_A;
    }

    return nextPlayer;
}
