// testGame.c
//
//
// Figure out how we'll create the Game and use it in main
// and pass parameters through the test functions.

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "Game.h"

#define DEFAULT_DISCIPLINES { STUDENT_BQN, STUDENT_MMONEY, STUDENT_MJ, STUDENT_MMONEY, STUDENT_MJ, STUDENT_BPS, STUDENT_MTV, STUDENT_MTV, STUDENT_BPS,STUDENT_MTV, STUDENT_BQN, STUDENT_MJ, STUDENT_BQN, STUDENT_THD, STUDENT_MJ, STUDENT_MMONEY, STUDENT_MTV, STUDENT_BQN, STUDENT_BPS }
#define DEFAULT_DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,7,9,2,8,10,5}

void testNewGame (void);
void testDisposeGame (void);
void testMakeAction (void);
void testThrowDice (void);
void testGetDiscipline (void);
void testGetDiceValue (void);
void testGetMostARCS (void);
void testGetMostPublications (void);
void testGetTurnNumber (void);
void testGetWhoseTurn (void);
void testGetCampus (void);
void testGetARC (void);
void testGetKPIpoints (void);
void testGetARCs (void);
void testGetGO8s (void);
void testGetCampuses (void);
void testGetIPs (void);
void testGetPublications (void);
void testGetStudents (void);
void testGetExchangeRate (void);
void testIsLegalAction (void);

void magicalResources (void);

int main (int argc, char *argv[]) {

    magicalResources ();

    printf ("Starting unit tests...\n");

    // Begin unit tests for each function
    testNewGame ();
    testDisposeGame ();
    testMakeAction ();
    testThrowDice ();
    testGetDiscipline ();
    testGetDiceValue ();
    testGetMostARCS ();
    testGetMostPublications ();
    testGetTurnNumber ();
    testGetWhoseTurn ();
    testGetCampus ();
    testGetARC ();
    testGetKPIpoints ();
    testGetARCs ();
    testGetGO8s ();
    testGetCampuses ();
    testGetIPs ();
    testGetPublications ();
    testGetStudents ();
    testGetExchangeRate ();
    testIsLegalAction ();

    printf ("All tests passed, you are Awesome!\n");

    return EXIT_SUCCESS;
}

//Roger
void testNewGame () {
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;

    // Test return value of newGame() is successfully assigned
    Game theGame = newGame (disciplines, dice);
    disposeGame (theGame);

    // Use getter functions to ensure properly initialized

    printf ("passed testNewGame\n");
}

//Kenneth
void testDisposeGame () {
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game theGame = newGame (disciplines, dice);
    assert(sizeof(theGame) > 0);
    disposeGame(theGame);

    printf ("passed disposeGame\n");
}

//Josh
void testMakeAction () {
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;

    Game theGame = newGame( disciplines, dice );

    // increment turn to UNI_A
    throwDice( theGame, 1 );

    // pass ---------------------------------------------------------
    // not sure if this does anything

    // build campus -------------------------------------------------
    action buildCampus;
    buildCampus.actionCode = BUILD_CAMPUS;
    strcpy(buildCampus.destination, "");

    int campusesA = getCampuses( theGame, UNI_A );
    int campusesB = getCampuses( theGame, UNI_B );
    int campusesC = getCampuses( theGame, UNI_C );
    makeAction( theGame, buildCampus );

    // UNI_A should have one campus
    assert( getCampuses( theGame, UNI_A ) == campusesA + 1 );

    // UNI_B and UNI_C should not gain campuses
    assert( getCampuses( theGame, UNI_B ) == campusesB );
    assert( getCampuses( theGame, UNI_C ) == campusesC );

    // build go8 ----------------------------------------------------
    action buildGO8;
    buildGO8.actionCode = BUILD_GO8;
    strcpy(buildGO8.destination, "");

    int go8sA = getGO8s( theGame, UNI_A );
    int go8sB = getGO8s( theGame, UNI_B );
    int go8sC = getGO8s( theGame, UNI_C );
    makeAction( theGame, buildGO8 );

    // UNI_A should have one GO8 campus
    assert( getGO8s( theGame, UNI_A ) == go8sA + 1);

    // UNI_B and UNI_C should not gain go8s
    assert( getGO8s( theGame, UNI_B ) == go8sB );
    assert( getGO8s( theGame, UNI_C ) == go8sC );

    // obtain arc ---------------------------------------------------
    action obtainARC;
    obtainARC.actionCode = OBTAIN_ARC;
    strcpy(obtainARC.destination, "R");

    int arcsA = getARCs( theGame, UNI_A );
    int arcsB = getARCs( theGame, UNI_B );
    int arcsC = getARCs( theGame, UNI_C );
    makeAction( theGame, obtainARC );

    // UNI_A should have one more ARC
    assert( getARCs( theGame, UNI_A ) == arcsA + 1 );

    // UNI_B and UNI_C should not gain arcs
    assert( getARCs( theGame, UNI_B ) == arcsB );
    assert( getARCs( theGame, UNI_C ) == arcsC );

    // get publication ----------------------------------------------
    action getPublication;
    getPublication.actionCode = OBTAIN_PUBLICATION;

    int pubsA = getPublications( theGame, UNI_A );
    int pubsB = getPublications( theGame, UNI_B );
    int pubsC = getPublications( theGame, UNI_C );
    makeAction( theGame, getPublication );

    // UNI_A should have one more publication
    assert( getPublications( theGame, UNI_A ) == pubsA + 1);

    // UNI_B and UNI_C should not gain publications
    assert( getPublications( theGame, UNI_B ) == pubsB );
    assert( getPublications( theGame, UNI_C ) == pubsC );


    // get ip patent ------------------------------------------------

    action getIP;
    getIP.actionCode = OBTAIN_IP_PATENT;

    int ipsA = getIPs( theGame, UNI_A );
    int ipsB = getIPs( theGame, UNI_B );
    int ipsC = getIPs( theGame, UNI_C );
    makeAction( theGame, getIP );

    // UNI_A should have one more ip patent
    assert( getIPs( theGame, UNI_A ) == ipsA + 1 );

    // UNI_B and UNI_C should not gain ip patents
    assert( getIPs( theGame, UNI_B ) == ipsB );
    assert( getIPs( theGame, UNI_C ) == ipsC );

    // --------------------------------------------------------------

    disposeGame( theGame );

    printf( "passed makeAction\n" );
}

//Sebastian
void testThrowDice () {
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;

    // throwDice() ROLE:
    // Increment global turn; allocate resources.

    // TEST 1: Test global turn is incrementing

    Game theGame = newGame (disciplines, dice);

    throwDice( theGame, 1 );
    assert( getTurnNumber(theGame) == 0 ); //(Noting initial turn number is -1)

    throwDice( theGame, 1 );
    assert( getTurnNumber(theGame) == 1 );

    throwDice( theGame, 1 );
    assert( getTurnNumber(theGame) == 2 );

    int i; //C98
    for( i = 0; i != 100; ++i )
        throwDice( theGame, i%12+1 );

    assert( getTurnNumber(theGame) == 102 );

    disposeGame( theGame );

    // TEST 2: Test resources are being allocated to players

    theGame = newGame (disciplines, dice);

    int prior1 = getStudents (theGame, UNI_B, STUDENT_BQN);
    throwDice( theGame, 9 );

    // UNI_B should receive 1 STUDENT_BQN on a roll of 9
    assert( getStudents( theGame, UNI_B, STUDENT_BQN ) == prior1+1);

    int prior2 = getStudents (theGame, UNI_C, STUDENT_MTV);
    prior1 = getStudents (theGame, UNI_C, STUDENT_MJ);
    throwDice( theGame, 8);

    // UNI_C should receive 1 STUDENT_MJ and 1 STUDENT_MTV on a roll of 8

    assert( getStudents( theGame, UNI_C, STUDENT_MJ) == prior1+1);

    assert( getStudents( theGame, UNI_C, STUDENT_MTV) == prior2+1);

    disposeGame( theGame );

    printf( "passed throwDice\n" );
}

//Roger
void testGetDiscipline () {
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;

    Game theGame = newGame (disciplines, dice);

    // Perhaps add more tests for all varying student types
    assert (getDiscipline (theGame, 0) == STUDENT_BQN);
    assert (getDiscipline (theGame, 16) == STUDENT_MTV);
    assert (getDiscipline (theGame, 4) == STUDENT_MJ);
    assert (getDiscipline (theGame, 13) == STUDENT_THD);

    disposeGame (theGame);

    printf ("passed testGetDiscpline\n");
}

//Kenneth
void testGetDiceValue () {
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game theGame = newGame (disciplines, dice);
    
    int i = 0;
    for (i=0; i < NUM_REGIONS; ++i){
        assert (getDiceValue (theGame, i) == dice[i]);
        assert (getDiscipline (theGame, i) == disciplines[i]);
    }
    
    disposeGame (theGame);
    
    /**
    //test first region, some in the middle and last region
    assert (getDiceValue (theGame, 0) == 9);
    assert (getDiceValue (theGame, 1) == 10);
    assert (getDiceValue (theGame, 3) == 12);
    assert (getDiceValue (theGame, 7) == 11);
    assert (getDiceValue (theGame, 17) == 10);
    **/
    
    
    //Catch games that are hardcoded.
    int customDisciplines[] = { STUDENT_BPS, STUDENT_MMONEY, STUDENT_MTV, STUDENT_MTV, STUDENT_MMONEY, 
                        STUDENT_BPS, STUDENT_MTV, STUDENT_BQN, STUDENT_BQN,STUDENT_MTV, STUDENT_BQN, 
                        STUDENT_MTV, STUDENT_BQN, STUDENT_THD, STUDENT_MJ, STUDENT_MMONEY, STUDENT_THD, 
                        STUDENT_BQN, STUDENT_BQN };
    int customDice[] = {2,7,6,4,7,9,3,3,3,11,12,6,2,7,8,2,9,5,6};
    theGame = newGame (customDisciplines, customDice);
    i = 0;
    for (i=0; i < NUM_REGIONS; ++i){
        assert (getDiceValue (theGame, i) == customDice[i]);
        assert (getDiscipline (theGame, i) == customDisciplines[i]);
    }
    disposeGame (theGame);
    printf ("passed testGetDiceValue\n");

}

//Josh
void testGetMostARCS () {
    // getMostARCs()
    // return the university that has the most arcs

    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;

    Game theGame = newGame (disciplines, dice);

    // increment to UNI_A's turn
    throwDice( theGame, 1 );

    // test when everyone has equal amount of arcs
    assert( getMostARCs( theGame ) == NO_ONE );

    action getARC;
    getARC.actionCode = OBTAIN_ARC;

    // give UNI_A an ARC
    strcpy(getARC.destination, "L");
    makeAction( theGame, getARC );

    assert( getMostARCs( theGame ) == UNI_A );

    // increment to UNI_B's turn
    throwDice( theGame, 1 );

    // give UNI_B 2 ARCS
    strcpy(getARC.destination, "LR");
    makeAction( theGame, getARC );
    strcpy(getARC.destination, "LRR");
    makeAction( theGame, getARC );

    assert( getMostARCs( theGame ) == UNI_B );

    // increment to UNI_C's turn
    throwDice( theGame, 1 );

    // give UNI_C an ARC
    strcpy(getARC.destination, "LRRL");
    makeAction( theGame, getARC );

    // UNI_B should still have the most ARCs
    assert( getMostARCs( theGame ) == UNI_B );

    disposeGame( theGame );

    printf ("passed testGetMostARCS\n");
}

//Sebastian
void testGetMostPublications () {
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;

    // getMostPublications()
    // return the university that has the most publications

    Game theGame = newGame (disciplines, dice);

    // Increment to UNI_A's turn (turn0=>NO_ONE)
    throwDice( theGame, 1 );

    // Test when no-one has any publications
    assert( getMostPublications( theGame ) == NO_ONE );

    action getPub;

    getPub.actionCode = OBTAIN_PUBLICATION;

    // Give UNI_A a publication
    makeAction( theGame, getPub );

    assert( getMostPublications( theGame ) == UNI_A );

    // Increment to UNI_B's turn
    throwDice( theGame, 1 );

    // Give UNI_B 2 publications
    makeAction( theGame, getPub );
    makeAction( theGame, getPub );

    assert( getMostPublications( theGame ) == UNI_B );

    // Increment to UNI_C's turn
    throwDice( theGame, 1 );

    // Give UNI_C a publication
    makeAction( theGame, getPub );

    // UNI_B still has the most
    assert( getMostPublications( theGame ) == UNI_B );

    disposeGame( theGame );

    printf ("passed testGetMostPublications\n");
}

//Roger
void testGetTurnNumber () {

    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;

    Game theGame = newGame (disciplines, dice);

    //Turn -1 before game starts
    assert (getTurnNumber (theGame) == -1);

    // Now turn 0
    // Confirm diceScore argument's purpose
    throwDice (theGame, 1);
    assert (getTurnNumber (theGame) == 0);

    throwDice (theGame, 3);
    assert (getTurnNumber (theGame) == 1);

    throwDice (theGame, 2);
    throwDice (theGame, 7);
    assert (getTurnNumber (theGame) == 3);

    disposeGame (theGame);

    printf ("passed testGetTurnNumber\n");
}

//Kenneth
void testGetWhoseTurn () {
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game theGame = newGame (disciplines, dice);

    //test initial state
    assert(getWhoseTurn(theGame) == NO_ONE);

    //test each Uni
    throwDice( theGame, 1 );
    assert(getWhoseTurn(theGame) == UNI_A);

    throwDice( theGame, 1 );
    assert(getWhoseTurn(theGame) == UNI_B);

    throwDice( theGame, 1 );
    assert(getWhoseTurn(theGame) == UNI_C);

    throwDice( theGame, 1 );
    assert(getWhoseTurn(theGame) == UNI_A);

    disposeGame (theGame);
    printf ("passed testGetWhoseTurn\n");
}

//Josh
void testGetCampus () {
    // getCampus()
    // get the contents of a given vertex

    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;

    Game theGame = newGame( disciplines, dice );
    action putCampus;

    putCampus.actionCode = BUILD_CAMPUS;

    // increment to UNI_A's turn
    throwDice( theGame, 1 );

    // initially  UNI_A's campus
    assert( getCampus( theGame, "" ) == CAMPUS_A );

    // put a campus
    strcpy(putCampus.destination, "RB");
    makeAction( theGame, putCampus );

    // should be a campus here
    assert( getCampus( theGame, "RB" ) == CAMPUS_A );

    disposeGame( theGame );

    printf ("passed testGetCampus\n");
}

//Sebastian
void testGetARC () {
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;

    // getARCs()
    // Get what exists at an edge.

    Game theGame = newGame (disciplines, dice);

    // Increment to UNI_A's turn (turn0=>NO_ONE)
    throwDice( theGame, 1 );

    // Initially, no ARCs here.
    assert( getARC( theGame, "LR" ) == VACANT_ARC );

    // Put an ARC just below the top campus A
    action putARC;
    putARC.actionCode = OBTAIN_ARC;
    strcpy(putARC.destination, "LR");

    makeAction( theGame, putARC );

    // Now there should be an ARC there.
    assert( getARC( theGame, "LR" ) == ARC_A );

    // Switch to UNI_B
    throwDice( theGame, 1 );

    // Give UNI_B an ARC somewhere
    strcpy(putARC.destination,"LRLR");
    makeAction( theGame, putARC );

    // UNI_B should own the ARC in that spot
    assert( getARC( theGame, "LRLR" ) == ARC_B );

    disposeGame( theGame );

    printf ("passed testGetARC\n");

}

//Roger
void testGetKPIpoints () {

    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;

    Game theGame = newGame (disciplines, dice);

    // initial KPI should be twenty
    assert (getKPIpoints (theGame, UNI_A) == 20);
    assert (getKPIpoints (theGame, UNI_B) == 20);
    assert (getKPIpoints (theGame, UNI_C) == 20);

    // moves current player to UNI_A
    throwDice (theGame, 1);

    action makeARC;
    makeARC.actionCode = OBTAIN_ARC;
    strcpy (makeARC.destination, "LR");
    makeAction (theGame, makeARC);

    // moves current player to UNI_B
    throwDice (theGame, 1);
    makeAction (theGame, makeARC);
    makeAction (theGame, makeARC);
    makeAction (theGame, makeARC);

    // moves current player to UNI_C
    throwDice (theGame, 1);
    makeAction (theGame, makeARC);
    makeAction (theGame, makeARC);

    assert (getKPIpoints (theGame, UNI_A) == 20+2);
    // most ARC grants prestige +10 KPI
    assert (getKPIpoints (theGame, UNI_B) == 20+6+10);
    assert (getKPIpoints (theGame, UNI_C) == 20+4);

    disposeGame (theGame);

    printf ("passed testGetKPIpoints\n");
}

//Kenneth
void testGetARCs () {
    //gets the number of ARC grants a player has

    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game theGame = newGame (disciplines, dice);

    //Initially there should be 0 arc grants per uni
    assert( getARCs( theGame, UNI_A ) == 0 );
    assert( getARCs( theGame, UNI_B ) == 0 );
    assert( getARCs( theGame, UNI_C ) == 0 );

    //cycle around back to UNI_A's second turn
    throwDice( theGame, 1 );
    throwDice( theGame, 1 );
    throwDice( theGame, 1 );
    throwDice( theGame, 1 );

    action getARC;
    getARC.actionCode = OBTAIN_ARC;

    //test UNI_A getting a grant
    strcpy( getARC.destination, "L" );
    makeAction (theGame, getARC);
    assert( getARCs( theGame, UNI_A ) == 1 );

    //test UNI_A getting another arc grant on same path
    strcpy( getARC.destination, "LR" );
    makeAction (theGame, getARC);
    assert( getARCs( theGame, UNI_A ) == 2 );

    //test UNI_A getting another arc grant but on different path
    strcpy( getARC.destination, "R" );
    makeAction (theGame, getARC);
    assert( getARCs( theGame, UNI_A ) == 3 );

    //go to UNI_B's turn
    throwDice( theGame, 1 );
    strcpy( getARC.destination, "RRLRL" ); //ask Stanley if this is correct. Based on map in instructions
    makeAction (theGame, getARC);
    assert( getARCs( theGame, UNI_B ) == 1 );

    disposeGame( theGame );
    printf("passed testGetARCs\n");
}

//Josh
void testGetGO8s () {
    // getGO8s()
    // get the amount of GO8 campuses a player has

    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;

    Game theGame = newGame( disciplines, dice );

    // should have no GO8s initially
    assert( getGO8s( theGame, UNI_A ) == 0);
    assert( getGO8s( theGame, UNI_B ) == 0);
    assert( getGO8s( theGame, UNI_C ) == 0);

    // increment turn to UNI_A
    throwDice( theGame, 1 );

    // give UNI_A a GO8 campus
    action putGO8;
    putGO8.actionCode = BUILD_GO8;
    strcpy( putGO8.destination, "LR" );

    makeAction( theGame, putGO8 );

    // UNI_A should now have 1 GO8 campus
    assert( getGO8s( theGame, UNI_A ) == 1);
    assert( getGO8s( theGame, UNI_B ) == 0);
    assert( getGO8s( theGame, UNI_C ) == 0);

    disposeGame( theGame );

    printf ("passed testGetGO8s\n");
}

//Sebastian
void testGetCampuses () {
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;

    // getCampuses()
    // Get the amount of campuses a player has

    Game theGame = newGame (disciplines, dice);

    // Switch to UNI_A
    throwDice( theGame, 1 );

    // Should start off with 2 campuses per player
    assert( getCampuses( theGame, UNI_A ) == 2 );

    // Give UNI_A another campus
    action putCampus;
    putCampus.actionCode = BUILD_CAMPUS;
    strcpy(putCampus.destination, "LR");

    makeAction( theGame, putCampus );

    // UNI_A should now have 3 campuses
    assert( getCampuses( theGame, UNI_A ) == 3);

    disposeGame( theGame );

    printf ("passed testGetCampuses\n");
}

//Roger
// Make sure to check that these functions are independently tested
// and doesn't rely on having a prerequisite number of students
// to perform certain actions (some other function deals with that)
void testGetIPs () {

    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;

    Game theGame = newGame (disciplines, dice);

    // moves current player to UNI_A
    throwDice (theGame, 1);

    // should begin with 0 IPs
    assert (getIPs (theGame, UNI_A) == 0);
    assert (getIPs (theGame, UNI_B) == 0);
    assert (getIPs (theGame, UNI_C) == 0);

    action getIP;
    getIP.actionCode = OBTAIN_IP_PATENT;
    makeAction (theGame, getIP);

    // moves current player to UNI_B
    throwDice (theGame, 5);

    // moves current player to UNI_C
    throwDice (theGame, 3);
    makeAction (theGame, getIP);
    makeAction (theGame, getIP);

    assert (getIPs (theGame, UNI_A) == 1);
    assert (getIPs (theGame, UNI_B) == 0);
    assert (getIPs (theGame, UNI_C) == 2);

    disposeGame (theGame);

    printf ("passed testGetIPs\n");
}

//Kenneth
void testGetPublications () {
    //returns the number of publications a uni has
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game theGame = newGame (disciplines, dice);

    //initially there are 0 publications
    assert( getPublications( theGame, UNI_A ) == 0 );
    assert( getPublications( theGame, UNI_B ) == 0 );
    assert( getPublications( theGame, UNI_C ) == 0 );

    //increment to UNI_B's turn - case 1: gets 1 publication
    throwDice( theGame, 1 );
    action getPub;
    getPub.actionCode = OBTAIN_PUBLICATION;
    makeAction (theGame, getPub);

    //increment to UNI_C's turn - case 2: gets 2 publication
    throwDice( theGame, 1 );
    makeAction (theGame, getPub);
    makeAction (theGame, getPub);

    //increment to UNI_A's turn - case 3: gets 0 publication
    throwDice( theGame, 1 );

    //test states
    assert( getPublications( theGame, UNI_A ) == 1 );
    assert( getPublications( theGame, UNI_B ) == 2 );
    assert( getPublications( theGame, UNI_C ) == 0 );

    disposeGame( theGame );

    printf("passed testGetIPs\n");
}

//Josh
void testGetStudents () {
    // getStudents()
    // return the number of studets of the specified discpline type the
    // specified player currently has

    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;

    Game theGame = newGame( disciplines, dice );

    // initially no one should have any students
    assert( getStudents( theGame, UNI_A, STUDENT_BPS ) == 3 );
    assert( getStudents( theGame, UNI_A, STUDENT_BQN ) == 3 );
    assert( getStudents( theGame, UNI_A, STUDENT_MTV ) == 1 );
    assert( getStudents( theGame, UNI_A, STUDENT_MJ ) == 1 );
    assert( getStudents( theGame, UNI_A, STUDENT_MMONEY ) == 1 );
    assert( getStudents( theGame, UNI_A, STUDENT_THD ) == 0 );

    assert( getStudents( theGame, UNI_B, STUDENT_BPS ) == 3 );
    assert( getStudents( theGame, UNI_B, STUDENT_BQN ) == 3 );
    assert( getStudents( theGame, UNI_B, STUDENT_MTV ) == 1 );
    assert( getStudents( theGame, UNI_B, STUDENT_MJ ) == 1 );
    assert( getStudents( theGame, UNI_B, STUDENT_MMONEY ) == 1 );
    assert( getStudents( theGame, UNI_B, STUDENT_THD ) == 0 );

    assert( getStudents( theGame, UNI_C, STUDENT_BPS ) == 3 );
    assert( getStudents( theGame, UNI_C, STUDENT_BQN ) == 3 );
    assert( getStudents( theGame, UNI_C, STUDENT_MTV ) == 1 );
    assert( getStudents( theGame, UNI_C, STUDENT_MJ ) == 1 );
    assert( getStudents( theGame, UNI_C, STUDENT_MMONEY ) == 1 );
    assert( getStudents( theGame, UNI_C, STUDENT_THD ) == 0 );

    disposeGame( theGame );

    printf ("passed testGetStudents\n");
}

//Sebastian
void testGetExchangeRate () {
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;


    // getExchangeRate()
    // Return the amount of type A students needed to create a type B

    Game theGame = newGame (disciplines, dice);

    // Switch to UNI_A
    throwDice( theGame, 1 );

    // Initially, UNI_A has no retraining centers.
    assert( getExchangeRate( theGame, UNI_A, STUDENT_MJ, STUDENT_BPS ) == 3 );

    // Neither does UNI_B
    assert( getExchangeRate( theGame, UNI_B, STUDENT_MTV, STUDENT_MMONEY ) == 3 );

    // TODO: Test for cases where retraining centers exist.

    disposeGame( theGame );

    printf ("passed testGetExchangeRate\n");
}

//Free reign
void testIsLegalAction () {
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game theGame = newGame (disciplines, dice);

    //nothing is legal during terra NULLius
    action nextAction;
    nextAction.actionCode = PASS;
    assert (isLegalAction(theGame, nextAction) == FALSE);

    //players are not allowed to OBTAIN_PUBLICATION
    throwDice( theGame, 1 );
    nextAction.actionCode = OBTAIN_PUBLICATION;
    assert (isLegalAction(theGame, nextAction) == FALSE);

    //players are not allowed to OBTAIN_IP_PATENT
    throwDice( theGame, 1 );
    nextAction.actionCode = OBTAIN_IP_PATENT;
    assert (isLegalAction(theGame, nextAction) == FALSE);

    //players are allowed to START_SPINOFF
    throwDice( theGame, 1 );
    
    printf ("%d\n", getWhoseTurn(theGame));
    printf ("MJ:%d, MTV:%d, M$:%d\n",
    getStudents (theGame, getWhoseTurn(theGame), 3),
    getStudents (theGame, getWhoseTurn(theGame), 4),
    getStudents (theGame, getWhoseTurn(theGame), 5));
    
    nextAction.actionCode = START_SPINOFF;
    assert (isLegalAction(theGame, nextAction) == TRUE);

    //test non-existant action code
    throwDice( theGame, 1 );
    nextAction.actionCode = 99;
    assert (isLegalAction(theGame, nextAction) == FALSE);

    //test path validity
    throwDice( theGame, 1 );
    nextAction.actionCode = OBTAIN_ARC;
    strcpy( nextAction.destination, "RRRRRR" );
    assert (isLegalAction(theGame, nextAction) == FALSE);
    

    
    disposeGame(theGame);


    //Test obtaining ARC
    //Case 1: Legal
    theGame = newGame(disciplines, dice);
    throwDice( theGame, 1 );
    nextAction.actionCode = OBTAIN_ARC;
    strcpy( nextAction.destination, "R" );
    assert (isLegalAction(theGame, nextAction) == TRUE);
    disposeGame(theGame);

    //Case 2: Not enough students
    theGame = newGame(disciplines, dice);
    throwDice( theGame, 1 );
    nextAction.actionCode = OBTAIN_ARC;
    strcpy( nextAction.destination, "R" );
    makeAction(theGame, nextAction);
    strcpy( nextAction.destination, "L" );
    makeAction(theGame, nextAction);
    strcpy( nextAction.destination, "R" );
    makeAction(theGame, nextAction);
    strcpy( nextAction.destination, "L" ); //no more students
    assert (isLegalAction(theGame, nextAction) == FALSE);
    disposeGame(theGame);

    //Test retraining students
    //Case 1: Legal
    theGame = newGame(disciplines, dice);
    throwDice (theGame, 1);
    nextAction.actionCode = RETRAIN_STUDENTS;
    nextAction.disciplineFrom = STUDENT_BPS;
    nextAction.disciplineTo = STUDENT_BQN;
    assert (isLegalAction(theGame, nextAction) == TRUE);
    disposeGame(theGame);

    //Case 2: Not enough students
    theGame = newGame(disciplines, dice);
    throwDice (theGame, 1);
    nextAction.actionCode = RETRAIN_STUDENTS;
    nextAction.disciplineFrom = STUDENT_BPS;
    nextAction.disciplineTo = STUDENT_BQN;
    makeAction(theGame, nextAction);
    //all 3 students used. No longer a legal action.
    assert (isLegalAction(theGame, nextAction) == FALSE);
    disposeGame(theGame);

    //Case 3: Use of retraining center
    //TODO

    //test placing campuses
    //Case 1: Adjacent Campus
    theGame = newGame(disciplines, dice);
    throwDice (theGame, 1);
    nextAction.actionCode = BUILD_CAMPUS;
    strcpy(nextAction.destination, "R");
    assert (isLegalAction(theGame, nextAction) == FALSE);
    disposeGame(theGame);
    
    //Case 2: Legal
    theGame = newGame(disciplines, dice);
    throwDice (theGame, 1);
    nextAction.actionCode = BUILD_CAMPUS;
    strcpy(nextAction.destination, "RL");
    assert (isLegalAction(theGame, nextAction) == TRUE);
    disposeGame(theGame);

    //Case 3: Trying to place on occupied vertex
    theGame = newGame(disciplines, dice);
    throwDice (theGame, 1);
    nextAction.actionCode = BUILD_CAMPUS;
    strcpy(nextAction.destination, "RL"); 
    makeAction(theGame, nextAction); //R is now occupied

    strcpy(nextAction.destination, "RL");
    assert (isLegalAction(theGame, nextAction) == FALSE);
   
    disposeGame(theGame);

    //Case 3: Not enough students
    theGame = newGame(disciplines, dice);
    throwDice (theGame, 1);
    nextAction.actionCode = RETRAIN_STUDENTS;
    nextAction.disciplineFrom = STUDENT_BPS;
    nextAction.disciplineTo = STUDENT_BQN;
    makeAction(theGame, nextAction);
    
    //Now there are 0 BPS students. Try placing a campus
    nextAction.actionCode = BUILD_CAMPUS;
    strcpy(nextAction.destination, "R");
    assert (isLegalAction(theGame, nextAction) == FALSE);
    disposeGame(theGame);
    
    //Case 4: Building a campus not next to an ARC
    // TODO: Implement this feature!
    // This will break a lot of our tests though.
    /*
    theGame = newGame(disciplines, dice);
    throwDice (theGame, 1);
    nextAction.actionCode = BUILD_CAMPUS;
    strcpy(nextAction.destination, "RLRL");
    assert (isLegalAction(theGame, nextAction) == FALSE);
    disposeGame(theGame);
    */

    //test upgrading to GO8
    //Case 1: No campus there in the first place
    theGame = newGame(disciplines, dice);
    throwDice (theGame, 1);
    nextAction.actionCode = BUILD_GO8;
    strcpy(nextAction.destination, "R");
    assert (isLegalAction(theGame, nextAction) == FALSE);
    disposeGame(theGame);

    //Case 2: Legal
    //TODO


    //test trying to access occupied vertex

    //test if trying to place campus at place not adjacent to a UNI's arc grant

    //test trying to place campus when you don't have enough students to pay for it

    printf("passed testLegalAction\n");
}


// Debugging, testing purposes, simulation
// Attempts to preload resources for testing
// Better if could control it somehow
// Problem #1, no new campuses built, so only get students from
// hexagons around each player's starting point

// Problems found:
// when dice rolls 7, Money students not turned into THDs
// no player starts with any students (initial students)
// areas around go8 campus should produce 2 students on roll
#include <time.h>
void magicalResources () {
    int disciplines[] = DEFAULT_DISCIPLINES;
    int dice[] = DEFAULT_DICE;
    Game theGame = newGame (disciplines, dice);

    int magicNumber = 100;

    int min = 1;
    int max = 12;
    srand (time(NULL));
    int output;

    while (magicNumber != 0) {
        output = min + (rand() % (int)(max-min+1));
        printf ("dice: %d\n", output);
        throwDice (theGame, output);

        printf ("UNI_A =>\n");
        printf ("THD:%d\nBPS:%d\nB?:%d\nMJ:%d\nMTV:%d\nMM:%d\n\n",
            getStudents (theGame, UNI_A, 0),
            getStudents (theGame, UNI_A, 1),
            getStudents (theGame, UNI_A, 2),
            getStudents (theGame, UNI_A, 3),
            getStudents (theGame, UNI_A, 4),
            getStudents (theGame, UNI_A, 5));

        printf ("UNI_B =>\n");
        printf ("THD:%d\nBPS:%d\nB?:%d\nMJ:%d\nMTV:%d\nMM:%d\n\n",
            getStudents (theGame, UNI_B, 0),
            getStudents (theGame, UNI_B, 1),
            getStudents (theGame, UNI_B, 2),
            getStudents (theGame, UNI_B, 3),
            getStudents (theGame, UNI_B, 4),
            getStudents (theGame, UNI_B, 5));

        printf ("UNI_C =>\n");
        printf ("THD:%d\nBPS:%d\nB?:%d\nMJ:%d\nMTV:%d\nMM:%d\n\n",
            getStudents (theGame, UNI_C, 0),
            getStudents (theGame, UNI_C, 1),
            getStudents (theGame, UNI_C, 2),
            getStudents (theGame, UNI_C, 3),
            getStudents (theGame, UNI_C, 4),
            getStudents (theGame, UNI_C, 5));

        magicNumber--;
    }

    disposeGame (theGame);

}

